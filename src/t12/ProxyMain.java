/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12;

import java.util.ArrayList;

/**
 *
 * @author Sam
 */
public class ProxyMain {
    
    public static void main(String[] args) {
        
        ArrayList<Image> kansio = new ArrayList();
        
        kansio.add(new ProxyImage("trophy1.jpg"));
        kansio.add(new ProxyImage("trophy2.jpg"));
        kansio.add(new ProxyImage("trophy3.jpg"));
        kansio.add(new ProxyImage("trophy4.jpg"));
        kansio.add(new ProxyImage("trophy5.jpg"));
        
        kansio.forEach((ok) -> {
            ok.showData();
            ok.displayImage();
        });
        
    }
    
}
