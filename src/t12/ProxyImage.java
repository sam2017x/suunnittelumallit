/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12;

/**
 *
 * @author Sam
 */
public class ProxyImage implements Image{
    
    private RealImage image;
    private String filename;
    
    public ProxyImage(String filename) {
        this.filename = filename;
        if (image == null) {
           image = new RealImage(filename);
        }
    }

    @Override
    public void displayImage() {
        image.displayImage();
    }

    @Override
    public void showData() {
        image.showData();
    }
    
    
    
}
