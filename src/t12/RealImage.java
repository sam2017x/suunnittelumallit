/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12;

/**
 *
 * @author Sam
 */
public class RealImage implements Image{
    
    private String filename = null;
    
    public RealImage(final String filename) {
        this.filename = filename;
        //loadImageFromDisk();
    }
    
    @Override
    public void displayImage() {
        System.out.println("Loading & Displaying " + filename);
    }
    
   
    private void loadImageFromDisk() {
        System.out.println("Loading   " + filename);
    }

    @Override
    public void showData() {
        System.out.println(filename);
        
    }

    
}
