/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sam
 */
public class Kokoonpano implements Laiteosa {
    
    List<Laiteosa> laiteLista = new ArrayList<>();
    int hinta;
    
    @Override
    public void addLaite(Laiteosa laite){
        hinta = hinta + laite.getHinta();
        laiteLista.add(laite);
    }
    
    @Override
    public void tulosta(){
        laiteLista.forEach((l) -> {
            //hinta = hinta + l.getHinta();
            l.tulosta();
        });
    }
    @Override
    public int getHinta(){
        return hinta;
    }
    
}
