/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sam
 */
public class Kotelo implements Laiteosa {
    
    int hinta;
    int original;
    List<Laiteosa> laiteLista = new ArrayList<>();
    
    public Kotelo (int hinta){
        this.original = hinta;
        this.hinta = hinta;
    }
    
    @Override
    public void tulosta(){
        System.out.println("Kotelo (yksin) " + original);
        
        laiteLista.forEach((ok) -> {
            ok.tulosta();
        });
        
        System.out.println("Kotelo (yhteensä) " + hinta);
        
    }
    
    @Override
    public void addLaite(Laiteosa laite){
        hinta = hinta + laite.getHinta();
        laiteLista.add(laite);
        
    }

    @Override
    public int getHinta() {
        return hinta;
    }
    
}
