/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t3;

/**
 *
 * @author Sam
 */
public class Verkkokortti implements Laiteosa{
    
    int hinta;
    
    public Verkkokortti (int hinta){
        this.hinta = hinta;
    }
    
    @Override
    public void tulosta(){
        System.out.println("Verkkokortti " +hinta);
    }
    
    @Override
    public void addLaite(Laiteosa laite){
        throw new RuntimeException("Ei onnistu :-( ");
    }
    @Override
    public int getHinta(){
        return hinta;
    }
}
