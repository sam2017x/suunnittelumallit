/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t2;

/**
 *
 * @author Sam
 */
public class Outfit {
    
    Housu housu;
    Hattu hattu;
    Paita paita;
    Takki takki;

    public Outfit(AbstractFactory factory) {
        housu = factory.makeHousu();
        hattu = factory.makeHattu();
        paita = factory.makePaita();
        takki = factory.makeTakki();
        
    }

    public void paalla(){
        System.out.print("\nMinulla on päälläni: " + housu + ", " + hattu + ", " + paita + ", " + takki);
    }
    /*public void createVaatteet(AbstractFactory factory){
        
        Housu housu = factory.makeHousu();
        Hattu hattu = factory.makeHattu();
        Paita paita = factory.makePaita();
        Takki takki = factory.makeTakki();
    }*/
    
}
