/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t2;

/**
 *
 * @author Sam
 */
public class BossFactory implements AbstractFactory {
    
    @Override
    public Housu makeHousu(){
        return new BossHousu();
    }
    @Override
    public Paita makePaita(){
        return new BossPaita();
    }
    @Override
    public Takki makeTakki(){
        return new BossTakki();
    }
    @Override
    public Hattu makeHattu(){
        return new BossHattu();
    }
    
}
