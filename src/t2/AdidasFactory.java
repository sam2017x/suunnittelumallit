/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t2;

/**
 *
 * @author Sam
 */
public class AdidasFactory implements AbstractFactory{
    
    @Override
    public Housu makeHousu(){
        return new AdidasHousu();
    }
    @Override
    public Paita makePaita(){
        return new AdidasPaita();
    }
    @Override
    public Takki makeTakki(){
        return new AdidasTakki();
    }
    @Override
    public Hattu makeHattu(){
        return new AdidasHattu();
    }
    
}
