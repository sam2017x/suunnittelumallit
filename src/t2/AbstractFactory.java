/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t2;

/**
 *
 * @author Sam
 */
public interface AbstractFactory {
    
    public abstract Housu makeHousu();
    public abstract Paita makePaita();
    public abstract Takki makeTakki();
    public abstract Hattu makeHattu();
    
}
