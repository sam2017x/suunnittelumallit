/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t10;

/**
 *
 * @author Sam
 */
public class Paalikko extends Korottaja {
    
    public Paalikko (double raja){
        this.raja = raja;
    }
    
    @Override
    protected void kasitteleKorotus(double uusiPalkka) {
        
        System.out.println("Paalikko hoiti.");
        
    }

    @Override
    protected void eiPysty() {
        
        System.out.println("Paalikko sucks... hakemus siirretään toimitusjohtajalle.");
        
    }
    
}
