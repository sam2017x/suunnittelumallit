/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t10;

/**
 *
 * @author Sam
 */
public abstract class Korottaja {
    
    protected double raja;
    protected Korottaja next;
    
    public void setNext(Korottaja ok){
        next = ok;
    }
    
    public void korotaPalkka(double vanhaPalkka, double uusiPalkka) {
        double prosentti = (uusiPalkka - vanhaPalkka) / vanhaPalkka * 100;

        if (prosentti < raja) {
            kasitteleKorotus(uusiPalkka);          
        } else {
            if (next != null) {
                eiPysty();
                next.korotaPalkka(vanhaPalkka, uusiPalkka);
            }
        }
    }
    
    abstract protected void kasitteleKorotus(double uusiPalkka);
    abstract protected void eiPysty();
        
        
    
}
