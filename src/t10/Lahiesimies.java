/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t10;

/**
 *
 * @author Sam
 */
public class Lahiesimies extends Korottaja{
    
    public Lahiesimies(double raja){
        this.raja = raja;
    }
    
    @Override
    protected void kasitteleKorotus(double uusiPalkka) {
        System.out.println("Lahiesimies hoiti.");
    }

    @Override
    protected void eiPysty() {
        System.out.println("Lahiesimies sucks... siirretään hakemus paalikolle.");
        
    }
    
}
