/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t8;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Sam
 */
public class Noppapeli extends Peli {
    private Scanner scanner = new Scanner(System.in);
    private ArrayList<Pelaaja> players = new ArrayList<>();

    @Override
    void initializeGame() {
        System.out.println("Ohjeet:");
        System.out.println("1. Jokainen pelaaja valitsee numeron väliltä 1-6 aina vuoron alussa.");
        System.out.println("2. Pelaajat heittävät noppaa vuorollaan.");
        System.out.println("3. Jos pelaajan heittämän nopan arvo on sama kuin numero, jonka alussa valitsi, hän tippuu pois pelistä.");
        for(int i=0; i<playersCount; i++) {
            /*int kon=0;
            while(kon>9 || kon<6) {
                System.out.print("Pelaaja " + i + ", valitse numero väliltä 1-6");
                kon = scanner.nextInt();
                scanner.nextLine();
            }*/
            Pelaaja p = new Pelaaja();
            //p.setKnockOutNumber(kon);
            players.add(p);
        }
    }

    @Override
    void makePlay(int player) {
        if (!players.get(player).isKnockedOut()) {
            System.out.println();
            System.out.println("Pelaajan " + player + " vuoro. Valitse numero! (1-6)");
            int valinta = scanner.nextInt();
            players.get(player).setKnockOutNumber(valinta);
            System.out.println("Heitetään noppaa... *PewPew*");
            int tulos = ThreadLocalRandom.current().nextInt(1, 6 + 1);
            System.out.println("Nopan tulos: " + tulos);
            //int randomNum2 = ThreadLocalRandom.current().nextInt(1, 6 + 1);
            //System.out.println("Dice nr2 gives: " + randomNum2);
            //int sum = randomNum1+randomNum2;
            //System.out.println("Dices sum is " + sum);
            if(players.get(player).getKnockOutNumber()==tulos) {
                System.out.println("Pelaaja " + player + " putosi pelistä.");
                players.get(player).setKnockedOut(true);
            }
            System.out.println("Pääsit jatkoon!");
        }
    }

    @Override
    boolean endOfGame() {
        int notKnockedOutPlayers = 0;
        for(int i=0; i<playersCount; i++) {
            if(!players.get(i).isKnockedOut()) {
                notKnockedOutPlayers++;
            }
        }
        if(notKnockedOutPlayers==1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    void printWinner() {
        for(int i=0; i<playersCount; i++) {
            if(!players.get(i).isKnockedOut()) {
                System.out.println("Pelaaja " + i + " voittaa!");
            }
        }
    }}
