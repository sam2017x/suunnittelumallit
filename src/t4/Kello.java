/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t4;

import java.util.Observable;
import java.util.Observer;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sam
 */
public class Kello extends Observable implements Runnable{
    
    GregorianCalendar aika;

    @Override
    public void run() {
        
        //String temp = "";
        
        while (true){
            
            aika = new GregorianCalendar();
            
            String temp = "";
            
            int h = aika.get(GregorianCalendar.HOUR_OF_DAY);
            int m = aika.get(GregorianCalendar.MINUTE);
            int s = aika.get(GregorianCalendar.SECOND);
            
            temp += ((h < 10) ? "0" : "") + h + ":";
            temp += ((m < 10) ? "0" : "") + m + ":";
            temp += ((s < 10) ? "0" : "") + s;
            
            setChanged();
            notifyObservers(temp);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Kello.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }
    
}
