/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t7;

/**
 *
 * @author Sam
 */
public final class Taso3 implements State{
    
    private static final State instance = new Taso3();
    
    private Taso3(){
        
    }

    @Override
    public void kukaMinaOlen(Digimon digimon) {
        System.out.println("Olen Tason 3 Digimon");
    }

    @Override
    public void liiku(Digimon digimon) {
        System.out.println("Liikun nopeasti.");
        
    }

    @Override
    public void hyokkaa(Digimon digimon) {
        System.out.println("Taistelen ampuma-aseilla");
    }

    @Override
    public void levelUp(Digimon digimon) {
        System.out.println("Olen jo Tason 3 Digimon, joten en voi enää kehittyä.");
        
    }
    
    public static State getInstance(){
        return instance;
    }
}
