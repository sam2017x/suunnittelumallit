/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t7;

/**
 *
 * @author Sam
 */
public interface State {
    
    public void kukaMinaOlen(Digimon digimon);
    public void liiku(Digimon digimon);
    public void hyokkaa(Digimon digimon);
    public void levelUp(Digimon digimon);
    
}
