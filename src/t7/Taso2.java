/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t7;

/**
 *
 * @author Sam
 */
public final class Taso2 implements State{
    
    private static final State instance = new Taso2();
    
    private Taso2(){
        
    }

    @Override
    public void kukaMinaOlen(Digimon digimon) {
        System.out.println("Olen Tason 2 Digimon.");
    }

    @Override
    public void liiku(Digimon digimon) {
        System.out.println("Liikun sukkelasti.");
        
    }

    @Override
    public void hyokkaa(Digimon digimon) {
        System.out.println("Taistelen miekoilla.");
    }

    @Override
    public void levelUp(Digimon digimon) {
        System.out.println("LEVEL UP!");
        digimon.setState(Taso3.getInstance());
        
    }
    
    public static State getInstance(){
        return instance;
    }
    
}
