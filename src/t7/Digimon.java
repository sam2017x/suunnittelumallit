/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t7;

/**
 *
 * @author Sam
 */
public class Digimon {
    
    private State state;
    
    public Digimon(){
        state = Taso1.getInstance();
    }
    
    public void liiku(){
        state.liiku(this);
    }
    
    public void hyokkaa(){
        state.hyokkaa(this);
    }
    public void kukaOlen(){
        state.kukaMinaOlen(this);
    }
    public void levelUp(){
        state.levelUp(this);
    }
    public void setState(State state){
        this.state = state;
    }
}
