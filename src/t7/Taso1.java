/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t7;

/**
 *
 * @author Sam
 */
public final class Taso1 implements State{
    
    private static final State instance = new Taso1();
    
    private Taso1(){
        
    }

    @Override
    public void kukaMinaOlen(Digimon digimon) {
        System.out.println("Olen Tason 1 Digimon");
    }

    @Override
    public void liiku(Digimon digimon) {
        System.out.println("Liikun hitaasti.");
        
    }

    @Override
    public void hyokkaa(Digimon digimon) {
        System.out.println("Taistelen nyrkeillä");
    }

    @Override
    public void levelUp(Digimon digimon) {
        System.out.println("LEVEL UP!");
        digimon.setState(Taso2.getInstance());
        
    }
    
    public static State getInstance(){
        return instance;
    }


    
}
