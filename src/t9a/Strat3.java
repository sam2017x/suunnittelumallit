/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t9a;

import java.util.List;

/**
 *
 * @author Sam
 */
public class Strat3 implements Strategy{
    
    @Override
    public String listToString(List lista){
        
        String ok = "";
        
        String[] temp = new String[lista.size()];
        lista.toArray(temp);
        
        for (int i = 0;i<temp.length;i++){
            ok += temp[i];
            if ((i+1)%3==0){
                ok += "\n";
            }
        }

        
        return ok;
    }
    
}
