/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t9a;

import java.util.List;

/**
 *
 * @author Sam
 */
public class Conv {
    
    private Strategy strategy;
    
    public Conv(Strategy a){
        strategy = a;
    }
    public String listToString(List list) {
        return strategy.listToString(list);
    }
    public void setStrategy(Strategy b) {
        strategy = b;
    }


    
}
