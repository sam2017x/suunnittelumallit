/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t9a;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Sam
 */
public class Strat1 implements Strategy{
    
    @Override
    public String listToString(List lista){
        String ok = "";
        
        Iterator iterator = lista.listIterator();
        
        while(iterator.hasNext()) {
            ok += iterator.next() + "\n";
        }
        
        return ok;
    }
    
}
