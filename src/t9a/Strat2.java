/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t9a;

import java.util.List;

/**
 *
 * @author Sam
 */
public class Strat2 implements Strategy{
    
    @Override
    public String listToString(List lista){
        
        String ok = "";
        
        for(int i=0; i<lista.size(); i++) {
            ok += lista.get(i);
            if((i+1)%2==0) {
                ok += "\n";
            }
        }

        
        return ok;
    }
    
}
