/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t6b;

/**
 *
 * @author Sam
 */
public class Decoration implements Crypt_IF {
    
    private Crypt_IF inner;
    
    public Decoration (Crypt_IF wat){
        inner = wat;
    }

    @Override
    public String encrypt(String ok) {
        inner.encrypt(ok);
        return ok;
    }

    @Override
    public String decrypt(String ok) {
        inner.decrypt(ok);
        return ok;
    }
    
}
