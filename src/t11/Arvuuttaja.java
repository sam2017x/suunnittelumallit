/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t11;

/**
 *
 * @author Sam
 */
import java.util.Random;

public class Arvuuttaja {
    
    Random temp;
    
    public Arvuuttaja(){
        temp = new Random();
    }
    
    public Memento liityPeliin(){
        
        return new Memento(temp.nextInt(8));
    }
    
    public static boolean arvaa(Memento memento, int arvaus){
        
        return memento.getNumero() == arvaus;
        
    }

    
}
