/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t11;

import java.util.Scanner;

/**
 *
 * @author Sam
 */
public class Asiakas extends Thread{
    
    private Memento memento;
    private String nimi;
    
    public Asiakas(String nimi){
        
        this.nimi = nimi;
    }
    
    public void setMemento(Memento memento){
        
        this.memento = memento;
    }
    
    @Override
    public void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Arvaappa numero (0-8)");
        while(!Arvuuttaja.arvaa(memento, scanner.nextInt())){
            System.out.println("Väärin!");
            System.out.println("Arvaappa numero (0-8)");
        }
        System.out.println("Voitit!");

    }

    
}
